package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.Map;

public interface ICommandRepository {

    @NotNull Map<String, AbstractCommand> getArguments();

    @NotNull Map<String, AbstractCommand> getCommands();

}
