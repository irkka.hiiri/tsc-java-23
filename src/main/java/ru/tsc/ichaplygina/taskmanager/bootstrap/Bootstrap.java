package ru.tsc.ichaplygina.taskmanager.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.repository.*;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.command.project.*;
import ru.tsc.ichaplygina.taskmanager.command.system.*;
import ru.tsc.ichaplygina.taskmanager.command.task.*;
import ru.tsc.ichaplygina.taskmanager.command.user.*;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandDescriptionEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandNameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.repository.*;
import ru.tsc.ichaplygina.taskmanager.service.*;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, authService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService, authService);

    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyString(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command.getNeedAuthorization()) authService.throwExceptionIfNotAuthorized();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    public void initCommands() {
        initTaskCommands();
        initProjectCommands();
        initUserCommands();
        initSystemCommands();
    }

    private void initData() {
        try {
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
            userService.add("vasya", "123", "vasya@domain", Role.USER, "Vassily", "Ivanovich", "Pupkin");
            userService.add("vova", "123", "vova@domain", Role.USER, "Vova", "V.", "P.");
            authService.login("root", "toor");
            taskService.add("en", "Sample Task");
            taskService.add("to", "Sample Task");
            taskService.add("tre", "Sample Task");
            taskService.add("fire", "Sample Task");
            taskService.add("fem", "Sample Task");
            projectService.add("Yksi", "Sample Project");
            projectService.add("Kaksi", "Sample Project");
            projectService.add("Kolme", "Sample Project");
            projectService.add("Nelja", "Sample Project");
            projectService.add("Viisi", "Sample Project");
            authService.logout();
            authService.login("vasya", "123");
            taskService.add("vasya task 1", "Sample Task");
            taskService.add("vasya task 2", "Sample Task");
            projectService.add("Vasya project 1", "Sample Project");
            projectService.add("Vasya project 2", "Sample Project");
            authService.logout();
            authService.login("vova", "123");
            taskService.add("vova task 1", "Sample Task");
            taskService.add("vova task 2", "Sample Task");
            projectService.add("vova project 1", "Sample Project");
            projectService.add("vova project 2", "Sample Project");
            authService.logout();
        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    public void initProjectCommands() {
        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectShowByNameCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectRemoveByNameCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectStartByNameCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCompleteByNameCommand());
    }

    public void initSystemCommands() {
        registerCommand(new InfoCommand());
        registerCommand(new AboutCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new ListCommandsCommand());
        registerCommand(new ListArgumentsCommand());
        registerCommand(new ExitCommand());
    }

    public void initTaskCommands() {
        registerCommand(new TaskListCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskShowByNameCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskRemoveByNameCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskStartByNameCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCompleteByNameCommand());
        registerCommand(new TaskListByProjectCommand());
        registerCommand(new TaskAddToProjectCommand());
        registerCommand(new TaskRemoveFromProjectCommand());
    }

    public void initUserCommands() {
        registerCommand(new LoginCommand());
        registerCommand(new LogoutCommand());
        registerCommand(new UserListCommand());
        registerCommand(new UserAddCommand());
        registerCommand(new UserShowByIdCommand());
        registerCommand(new UserShowByLoginCommand());
        registerCommand(new UserUpdateByIdCommand());
        registerCommand(new UserUpdateByLoginCommand());
        registerCommand(new ChangePasswordCommand());
        registerCommand(new ChangeRoleCommand());
        registerCommand(new UserRemoveByIdCommand());
        registerCommand(new UserRemoveByLoginCommand());
        registerCommand(new UserLockByIdCommand());
        registerCommand(new UserLockByLoginCommand());
        registerCommand(new UserUnlockByIdCommand());
        registerCommand(new UserUnlockByLoginCommand());
        registerCommand(new WhoAmICommand());
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        initData();
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {
            @NotNull final String terminalCommand = command.getCommand();
            @NotNull final String commandDescription = command.getDescription();
            @Nullable final String commandArgument = command.getArgument();
            if (isEmptyString(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyString(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyString(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initCommands();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
