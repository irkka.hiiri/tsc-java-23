package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserNotLoggedInException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User not logged in.";

    public UserNotLoggedInException() {
        super(MESSAGE);
    }

}
