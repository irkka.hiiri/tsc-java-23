package ru.tsc.ichaplygina.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "exit";

    @NotNull
    private final static String DESCRIPTION = "quit";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        System.exit(0);
    }

}
